"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import logging


###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
#client = MongoClient(host='0.0.0.0', port=8000)
db = client.tododb
db.tododb.drop()
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('distance', 1000, type=int)
    date = request.args.get('begin_date', 'You messed up', type=str)
    time = request.args.get('begin_time', 'You messed up', type=str)

    #app.logger.debug("dist={}".format(dist))
    #app.logger.debug("date={}".format(date))
    #app.logger.debug("time={}".format(time))
    #app.logger.debug("km={}".format(km))

    combined = date + ' ' + time
    datetime = arrow.get(combined, 'YYYY-MM-DD HH:mm')

    #app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, dist, datetime.isoformat())
    close_time = acp_times.close_time(km, dist, datetime.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/_display')
def _display():
    _items = db.tododb.find()
    items = [item for item in _items]
    db.tododb.drop()    # Clear out for the next time it gets displayed
    return render_template('display.html', items=items)


@app.route('/_submit')
def _submit():
    # app.logger.debug("Begin submit function")
    # print(request.args.to_dict())
    km = request.args.get('kilometers', 996, type=float)
    o = request.args.get('open_time', "You fucked up", type=str)
    close = request.args.get('close_time', "You fucked up", type=str)
    item_doc = {
        'location': km,
        'open': o,
        'close': close
    }
    db.tododb.insert_one(item_doc)
    # app.logger.debug("End submit function")
    return redirect(url_for("index"))  # Don't do anything to the page on submit (only update the database)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
