# Nose tests #

Run as "nosetests" from the "brevets" directory.

Does not work from repo main directory (tests not found by
Nose). 

Does not work from within brevets/tests. 

If run within the "tests" subdirectory, import errors will occur because
the path does not include the files to be imported.
